#!/bin/bash
echo "Création du script de configuration bin/configure_streamlit.sh de Streamlit."
lib/altgr-streamlit/bin/create_configure_script.sh "$1"

echo "Création du script d'exécution bin/run_streamlit.sh de Streamlit."
lib/altgr-streamlit/bin/create_run_script.sh "$1"

echo "Création du script d'exécution bin/generate_streamlit_pages.sh de Streamlit."
lib/altgr-streamlit/bin/create_generate_script.sh "$1"
