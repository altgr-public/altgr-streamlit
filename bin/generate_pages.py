#!/usr/bin/env python
import argparse

from altgr_streamlit.generate_pages import generate_pages


def run_cli() -> None:
    parser = get_parser()
    args = parser.parse_args()
    generate_pages(args.filepath)


def get_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        prog='generate_pages',
        description='Generate all Streamlit pages from pages.py file.'
    )
    parser.add_argument('filepath')
    return parser


if __name__ == '__main__':
    run_cli()
