FILEPATH=bin/generate_streamlit_pages.sh
cat > $FILEPATH <<EOF
#!/bin/bash
echo "Création des pages Streamlit à partir du fichier $1."
pipenv run lib/altgr-streamlit/bin/generate_pages.py $1
EOF
chmod +x $FILEPATH
