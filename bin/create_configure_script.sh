FILEPATH=bin/configure_streamlit.sh
cat > $FILEPATH <<EOF
#!/bin/bash
echo "Chargement des variables d'environnement."
set -a
source .env

echo "Valeur par défaut pour STREAMLIT_SERVER_ADDRESS : localhost."
STREAMLIT_SERVER_ADDRESS="${STREAMLIT_SERVER_ADDRESS:=localhost}"

echo "Remplacement des variables d'environnement dans le gabarit .streamlit/config.toml.tpl"
envsubst < .streamlit/config.toml.tpl > .streamlit/config.toml

if test -f ".streamlit/secrets.toml"; then
   echo "Remplacement des variables d'environnement dans le gabarit .streamlit/secrets.toml.tpl"
   envsubst < .streamlit/secrets.toml.tpl > .streamlit/secrets.toml
fi
EOF
chmod +x $FILEPATH
