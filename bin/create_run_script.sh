FILEPATH=bin/run_streamlit.sh
cat > $FILEPATH <<EOF
#!/bin/bash
pipenv run streamlit run $1
EOF
chmod +x $FILEPATH
