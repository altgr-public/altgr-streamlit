"""
Bootstrap tasks.
"""
import locale
from typing import Callable

import streamlit as st

from altgr_streamlit.layout import set_layout
from altgr_streamlit.router import Pages, page_from_filepath, set_router


locale.setlocale(locale.LC_ALL, '')


def bootstrap(pages: Pages, filepath: str) -> Callable:
    """Todo: Add middlewares and layouts."""
    separator = f"\n{''.join(['*'] * 80)}\n"
    msg = "Démarrage de l'application streamlit Serge Ferrari"
    print(f'{separator}{msg}{separator}')
    # print(f'filepath : {filepath}')
    page = page_from_filepath(pages, filepath)
    set_layout(page)
    # Ne peut être effectué avant st.set_page_config dans set_layout
    set_router(pages)
    return page['view']
