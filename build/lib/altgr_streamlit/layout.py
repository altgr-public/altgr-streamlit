"""
Layout.
"""
import streamlit as st

from altgr_streamlit.router import Page


def set_layout(page: Page) -> None:
    # print(f"[set_page_layout] page : {page}")
    initial_sidebar_state = get_initial_sidebar_state(page)
    title_or_header = page.get('title', page.get('header'))
    st.set_page_config(
        page_title=title_or_header,
        page_icon=page.get('icon'),
        layout=page.get('layout', 'centered'),
        initial_sidebar_state=initial_sidebar_state
    )
    header_or_title = page.get('header', page.get('title'))
    st.title(header_or_title)


def get_initial_sidebar_state(page: Page) -> str:
    sidebar_state = page.get('initial_sidebar_state')
    valid_states = ['auto', 'expanded', 'collapsed']
    if sidebar_state is None:
        return 'auto'
    elif isinstance(sidebar_state, str) and sidebar_state in valid_states:
        return sidebar_state
    elif callable(sidebar_state):
        return sidebar_state()
    else:
        raise ValueError(f"Invalid initial_sidebar_state : {sidebar_state}")
