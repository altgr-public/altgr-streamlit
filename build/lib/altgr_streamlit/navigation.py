"""
Navigation buttons.
"""
import streamlit as st

from altgr-streamlit.router import switch_page


def go_to_dashboard():
    dashboard = st.button(
        f":bar_chart: Charger votre tableau de bord",
        use_container_width=True
    )
    if dashboard:
        try:
            load_dataset()
            switch_page('history')
        except Exception as err:
            st.error("Erreur de chargement du jeux de données.")
            print("[load_dataset]", err)
            st.stop()


def go_to_upload_page():
    new_data = st.button("\U0001F4C4 Ajouter de nouvelles données")
 #   st.write("Mettre à jour mon dashboard avec des nouvelles données.")
    if new_data:
        switch_page('upload')
