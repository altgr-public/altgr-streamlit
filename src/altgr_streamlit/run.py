"""
Runs streamlit.
"""
import inspect
import locale
from typing import Any, Callable

import streamlit as st

from altgr_streamlit.layout import set_layout
from altgr_streamlit.router import Pages, page_from_filepath, set_router


locale.setlocale(locale.LC_ALL, '')


def run(pages: Pages, filepath: str) -> None:
    """Sets Streamlit, gets the view and displays it.

    Todo: Add middlewares and layouts.
    """
    separator = f"\n{''.join(['*'] * 80)}\n"
    msg = "Démarrage de l'application streamlit"
    print(f'{separator}{msg}{separator}')
    # print(f'filepath : {filepath}')
    page = page_from_filepath(pages, filepath)
    set_layout(page)
    # Ne peut être effectué avant st.set_page_config dans set_layout
    set_router(pages)
    value = get_value(pages, page)
    handle(page['view'], value)


def get_value(pages: Pages, page: dict[str, Any]) -> Any:
    if 'bootstrap' in page:
        if page['bootstrap'] is None:
            return None
        else:
            return page['bootstrap']()
    elif 'default' in pages and 'bootstrap' in pages['default']:
        return pages['default']['bootstrap']()
    else:
        return None


def handle(view: Callable, value: Any):
    nbr_args = len(inspect.getfullargspec(view).args)
    if nbr_args == 1:
        return view(value)
    elif nbr_args == 0:
        return view()
    else:
        raise TypeError("The function {view} expects more than one parameter.")
