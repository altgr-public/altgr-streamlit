"""
Router.
"""
from importlib import import_module
import os.path
from typing import Any, Callable

import streamlit as st
from streamlit.runtime.scriptrunner import RerunData, RerunException
from streamlit.source_util import get_pages


Page = dict[str, Any]
Pages = dict[str, Page]


def set_router(pages: Pages) -> None:
    if 'pages' not in st.session_state:
        st.session_state['pages'] = pages


def page_from_filepath(pages: Pages, filepath: str) -> Page:
    label = label_from_filepath(filepath)
    # print(f"[page_from_filepath] label : {label}")
    for page in pages.values():
        # print(f"[page_from_filepath] page : {page}")
        if page.get('label') == label:
            page_default = pages.get('default', {})
            return page_default | page
    else:
        raise ValueError(f"Page manquante : {label}")


def label_from_filepath(filepath: str) -> str:
    """Returns the label of a page from its filepath."""
    # print(f"[label_from_filepath] filepath : {filepath}")
    basename = os.path.basename(filepath)
    if basename[0].isdigit():
        basename = basename[basename.find('_') + 1:]
    basename = basename.replace('_', ' ')
    # print(f"[label_from_filepath] basename : {basename}")
    return os.path.splitext(basename)[0]


def switch_page(page_name: str) -> None:
    """Switches page programmatically in a multipage application."""
    filename = filename_from_page_name(page_name)
    filepath = get_index_filepath()
    pages = get_pages(filepath)
    for page_hash, config in pages.items():
        if config['page_name'] == filename:
            raise RerunException(RerunData(
                page_script_hash=page_hash,
                page_name=filename
            ))
            break
    else:
        filenames = [config['page_name'] for config in pages.values()]
        msg = f"Could not find page {filename}. Must be one of {filenames}"
        raise ValueError(msg)


def get_url_path(base_url: str, page_name: str) -> str:
    label = st.session_state['pages']['page_name']['label']
    print(f"[get_url] label : {label}")
    formatted = label.replace(' ', '_')
    print(f"[get_url] formatted : {formatted}")
    return f'{base_url}/{formatted}'
    

# def guard(callback: Callable, page_name: str) -> None:
#     """Requires authentication before visiting pages.

#     Checks the reauthentication cookie is user is not authenticated .
#     """
#     if is_authenticated():
#         log(f"[guard] profile : {st.session_state['profile']}")
#         monitor_visit(st.session_state['id_session'], page_name)
#         callback()
#     else:
#         log("[guard] not authenticated")
#         authenticate()
