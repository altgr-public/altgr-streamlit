import importlib.util
import os
import os.path
import shutil
from typing import Any


def generate_pages(filepath: str) -> None:
    dir_streamlit = os.path.dirname(filepath)
    module_name = get_module_name(dir_streamlit)
    package = get_package(dir_streamlit)
    module = importlib.import_module(module_name, package)
    generate_from_dict(module.pages, dir_streamlit)


def get_module_name(dir_streamlit: str) -> str:
    """Returns module name under Python's syntax.

    Removes 'src' prefix, replaces os.sep by dot and removes extension.
    """
    head, tail = os.path.split(dir_streamlit)
    module_name = os.path.splitext(tail)[0]
    while head != 'src':
        head, tail = os.path.split(head)
        module_name = '.'.join([tail, module_name])
    return f'{module_name}.view.pages'


def get_package(filepath: str) -> str:
    """Returns package name (in 'src' directory)."""
    head, _ = os.path.split(filepath)
    while head != 'src':
        head, tail = os.path.split(head)
    return tail


def generate_from_dict(pages: dict[str, Any], dir_streamlit: str) -> None:
    create_directory_pages(dir_streamlit)
    create_init(dir_streamlit)
    set_positions(pages)
    for key, value in pages.items():
        if key == 'default':
            continue
        else:
            generate_page(dir_streamlit, key, value)


def set_positions(pages: dict[str, Any]) -> None:
    """Sets positions if missing.

    Todo: checks that positions are unique.
    """
    print(f"[set_positions] pages : {pages}")
    positions = [
        page['position']
        for page in pages.values()
        if 'position' in page
    ]
    print(f"positions : {positions}")
    nbr = len([key for key in pages.keys() if key != 'default'])
    print(f"nbr : {nbr}")
    missing = [i for i in range(2, nbr + 1) if i not in positions]
    print(f"missing : {missing}")
    for key, value in pages.items():
        if 'position' not in value and key not in ['default', 'index']:
            value['position'] = missing.pop()
    print(f"after : {pages}")
    

def create_directory_pages(dir_streamlit: str) -> None:
    dir_pages = os.path.join(dir_streamlit, 'pages')
    if os.path.exists(dir_pages):
        print(f"Suppression du répertoire {dir_pages}")
        shutil.rmtree(dir_pages)
    print(f"Création du répertoire {dir_pages}")
    os.makedirs(dir_pages)


def create_init(dir_streamlit: str) -> None:
    filepath = os.path.join(dir_streamlit, 'pages', '__init__.py')
    with open(filepath, 'a'):
        pass


def generate_page(dir_streamlit: str, key: str, value: dict[str, Any]) -> None:
    filename = f"{value['label']}.py"
    if key != 'index':
        filename = f"{value['position']}_{filename}"
        filename = os.path.join('pages', filename)
    filepath = os.path.join(dir_streamlit, filename)
    module_name = get_module_name(dir_streamlit)
    write_stub(filepath, module_name)
    print(f"Page {filepath} créée.")


def write_stub(filepath: str, module_name: str) -> None:
    content = f"""from altgr_streamlit.run import run
from {module_name} import pages


run(pages, __file__)"""
    with open(filepath, 'w') as handle:
        handle.write(content)

