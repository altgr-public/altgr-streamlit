from datetime import datetime, time, timedelta

import pandas as pd
import streamlit as st


Interval = dict[str, pd.Timestamp]


def slider_interval(df: pd.DataFrame) -> Interval:
    """Dates are picked in datetime and casted to pd.Timestamp for better
    interaction with pandas."""
    (start, stop) = st.slider(
        'Intervalle temporel',
        value=(datetime(2023, 5, 1), datetime(2023, 6, 1)),
        min_value=df.index.min().to_pydatetime(),
        max_value=df.index.max().to_pydatetime(),
        format='DD/MM/YYYY kk:mm',
        step=timedelta(minutes=15)
    )
    return {'start': pd.Timestamp(start), 'stop': pd.Timestamp(stop)}


def picker_interval(interval: Interval | None = None) -> Interval:
    """Dates are picked in datetime and casted to pd.Timestamp for better
    interaction with pandas."""
    if interval is None:
        day_default = datetime(2023, 5, 5)
        value = (0, 24)
    else:
        start = interval['start'] 
        day_default = start.date()
        diff_hours = int((interval['stop'] - start).total_seconds() // 3600)
        value = (start.hour, diff_hours + start.hour)
    day = st.date_input('Jour de début', day_default)
    anchor = datetime.combine(day, datetime.min.time())
    (start_hour, stop_hour) = st.slider(
        'Tranche horaire',
        value=value,
        min_value=0,
        max_value=96,
        step=1
    )
    interval = {
        'start': pd.Timestamp(anchor + timedelta(hours=start_hour)),
        'stop': pd.Timestamp(anchor + timedelta(hours=stop_hour))
    }
    return interval


def picker_duration() -> int:
    start_hour = st.time_input('Heure de début', datetime.min.time())
    return st.slider(
        'Durée (heures)',
        value=10,
        min_value=1,
        max_value=96,
        step=1
    )
