#!/usr/bin/env python
import codecs
import os.path
from setuptools import setup


def read(rel_path):
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, rel_path), 'r') as fp:
        return fp.read()


def get_version(name: str) -> str:
    """Correct format:
    __version__ = "va.b.c"
    Beware of spaces before and after '='!
    """
    std_name = standardise_name(name)
    filepath = os.path.join('src', std_name, '_version.py')
    line = read(filepath).splitlines()[0]
    if line.startswith('__version__'):
        tag = line.split('=')[1]
        return tag[2:-1]
    else:
        raise RuntimeError("Unable to find version string.")


def standardise_name(name: str) -> str:
    """Returns standard Python name for the module."""
    return name.lower().replace('-', '_')


NAME = 'altgr-streamlit'


setup(
    name=NAME,
    version=get_version(NAME),
    description="Routines pour les spécifications Vega Lite",
    long_description=open('README.md').read(),
    author='AltGR',
    author_email='antoine@alt-gr.tech',
    license='?',
    packages=[
        'altgr_streamlit',
        'altgr_streamlit.widget',
    ],
    package_dir={'': 'src'},
    python_requires='>=3.11'
)
